<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public const Admin = 'Admin';
    public const Company = 'Company';
    public const Employee = 'Employee';

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->using('App\Models\UserRole');
    }
}
