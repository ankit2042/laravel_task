-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2019 at 08:20 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CRM_Laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_08_062413_create_roles_table', 1),
(5, '2019_11_08_062608_create_user_roles_table', 1),
(6, '2019_11_11_124252_create_notifications_table', 2),
(7, '2019_11_12_142249_create_websockets_statistics_entries_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('apurohit@vkaps.com', '$2y$10$PQC5gbp7tQrDfuBNTrOcBudxtKD3bFySbqTCn/5W.jQyddcz1mXvi', '2019-11-15 04:19:38'),
('ankit.purohit2042@gmail.com', '$2y$10$JlnYe8Io4/qQEKyQomec9OdnySax8Q4d7YCuQ.MwWBO...mNwg1Vq', '2019-11-15 04:38:06');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2019-11-08 07:41:42', '2019-11-08 07:41:42'),
(2, 'Company', '2019-11-08 07:41:42', '2019-11-08 07:41:42'),
(3, 'Employee', '2019-11-08 07:41:49', '2019-11-08 07:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '2019-11-13 08:01:06', '2019-11-13 08:01:06'),
(2, '3', '2', '2019-11-13 08:02:52', '2019-11-13 08:02:52'),
(3, '4', '3', '2019-11-13 08:03:56', '2019-11-13 08:03:56'),
(4, '5', '3', '2019-11-13 08:05:59', '2019-11-13 08:05:59'),
(5, '6', '3', '2019-11-15 08:26:09', '2019-11-15 08:26:09'),
(6, '8', '3', '2019-11-15 11:03:41', '2019-11-15 11:03:41'),
(7, '9', '2', '2019-11-15 11:26:10', '2019-11-15 11:26:10'),
(8, '10', '3', '2019-11-15 12:03:16', '2019-11-15 12:03:16'),
(9, '11', '2', '2019-11-15 14:31:25', '2019-11-15 14:31:25'),
(10, '12', '3', '2019-11-15 14:42:08', '2019-11-15 14:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `website`, `email_verified_at`, `password`, `company`, `role`, `logo`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin Name', NULL, NULL, 'admin@admin.com', NULL, NULL, '$2y$10$4QZS0dlLEqwWzOiM5gUbaOfEbXwWSSlQ5L21g3BvX8Uoe9v/OxGBW', NULL, '1', NULL, NULL, 'QCg2h9T54ikxteiPTJub2omW9LOL3GeGvOuRQnhULTP9g47SwJOG3Tggk5l0', '2019-11-13 02:28:46', '2019-11-15 05:55:13'),
(4, 'A Ankitt Purohit', 'A Ankitt', 'Purohit', 'apurohit@vkaps.com', NULL, NULL, '$2y$10$fQRBCLlAfi5Vg2kl1RG2c.N6pbJ9uwEBRhFI4vm0XKKdsyfj6uteS', '3', '3', NULL, '1234456789', 'BanwmUnyjDsOKGMFVDsFM5RSk0sUiRxqM415nxSLnU0sfzEhSiuR60KpiFhd', '2019-11-13 02:33:51', '2019-11-15 05:49:38'),
(5, 'Ankit Purohit', 'Ankit', 'Purohit', 'dinesh@vkaps.com', NULL, NULL, '$2y$10$zu39g/4vfZk4eeKMIq/lEO9N6ZLbgZwnnTc2WyVGSA87BtyFX8OLa', '3', '3', NULL, '123454879', 'w0mRC7uCtEcpfCCDWs6UkCxsWQoqGhuf7TGEEyMoRwHrW7r97r5KL9RV2V4b', '2019-11-13 02:35:54', '2019-11-13 02:35:54'),
(6, 'Ankit sharma', 'Ankit', 'sharma', 'ankit.purohit2042@gmail.com', NULL, NULL, '$2y$10$l8Y7msvURhvTTNb7u9t/fuiFJ3Rn/61m1IgIbSPmvrXMA4lf3VwCm', '3', '3', NULL, '09678778985', 'OZKA5RtJkfVHSzzZTDakDZTRWv9sqO5WX3hQojiAxNN0OLf8dJUki9KmUVi6', '2019-11-15 02:56:02', '2019-11-15 04:26:17'),
(8, 'Vishu Sharma', 'Vishu', 'Sharma', 'vishu.sharma@gmail.com', NULL, NULL, '$2y$10$ZzKab34oHThMiwH1YBcfKe0z1xzylOpq4jviBJNs9/TKJNMIbjY0a', '3', '3', NULL, '13246497', NULL, '2019-11-15 05:33:41', '2019-11-15 05:33:41'),
(9, 'GPH', NULL, NULL, 'hr@gph.com', 'www.GPH.com', NULL, '$2y$10$bbE/V1m7Te3C84YYemZby.vsVf2HRQ6LUfcB2e04sWdF8MCxAYTfq', NULL, '2', 'thumb_1573817776.jpeg', '01234567899', 'NvIOBV8zHNvwviIgmMOsYDGTM6xK40oIKGlNMCiNEPiscQhz8fdXo6uEL1Y1', '2019-11-15 05:56:10', '2019-11-15 06:06:16'),
(10, 'palash agrawal', 'palash', 'agrawal', 'palash@geeksperhour.com', NULL, NULL, '$2y$10$.KEjipbr1QbYaBtMXaaQ2exfX9eoJ9D1MViX000MtU0PFJ4TxWltK', '9', '3', NULL, '02145646466', NULL, '2019-11-15 06:33:16', '2019-11-15 06:33:16'),
(11, 'Future Technology', NULL, NULL, 'a@futuretechnology.com', 'www.futuretechnology.com', NULL, '$2y$10$hoybki6LGPlnRfPyQfQFZuULG1rHeFVE6YrxHUO2F23CmPekaSK66', NULL, '2', 'thumb_1573828284.jpeg', '02145646466', NULL, '2019-11-15 09:01:24', '2019-11-15 09:01:24'),
(12, 'kishan kewat', 'kishan', 'kewat', 'kishan@geeksperhour.com', NULL, NULL, '$2y$10$NJDVZ7P47L5vtZ0AKFZNueDD.8iRSlApMK/kfeDUe8JMRNKhuX.r2', '9', '3', NULL, '02145646466', NULL, '2019-11-15 09:12:08', '2019-11-15 09:12:08');

-- --------------------------------------------------------

--
-- Table structure for table `websockets_statistics_entries`
--

CREATE TABLE `websockets_statistics_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_website_unique` (`website`);

--
-- Indexes for table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
