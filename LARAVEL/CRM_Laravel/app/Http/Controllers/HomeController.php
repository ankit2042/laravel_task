<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\BroadcastNotification;
use App\Notifications\DatabaseNotification;
use Notification;
use App;
use Auth;
use Session;
use App\Models\User;
use App\Models\UserRole;
class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');   
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$user_id = Auth::user()->id;
		$total_company=User::where('role','2')->count();
		$total_employee=User::where('role','3')->count();

		$company_id = auth()->user()->id;
		$company_employee = User::where('company',$company_id)->count();
		return view('home')->with(['total_company'=>$total_company,'total_employee'=>$total_employee,'company_employee'=>$company_employee]);
	}

	public function lang($locale){
		App::setLocale($locale);
		session()->put('locale', $locale);
		return redirect()->back();
	}

	public function notify(){
		$auth_user_id = Auth::User()->id;
		if($auth_user_id == 1){
			$user = User::where('role','=',2)->get();
			$letter = collect(['title'=>'New company add in our IT Park Indore']);
			Notification::send($user,new DatabaseNotification($letter));
			return redirect()->route('company.company-list');
		}else{
			$user = User::where('company',$auth_user_id)->get();
			$user_name = Auth::User()->name;
			$letter = collect(['title'=>'New employee add '.$user_name]);
			Notification::send($user,new DatabaseNotification($letter));
			return redirect()->route('employee.employee-list');
		}
	}
}
