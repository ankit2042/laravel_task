@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('messages.dashboard')}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::user()->hasRole('Admin'))
                        <p><strong>Total Company :</strong>  {{ $total_company }} </p> <hr>
                        <p><strong>Total Employee :</strong> {{ $total_employee }}</p><hr>
                    @elseif(Auth::user()->hasRole('Company'))
                        <strong>Total Employee :</strong>  {{ $company_employee }}
                    @else
                        {{Auth::user()->name}} are logged in!
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
