<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// admin login page
Route::get('admin', ['as'=>'admin', function() {
     return view('auth/login');
}]);

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
Route::middleware(['auth','middleware' => 'App\Http\Middleware\CompanyMiddleware'])->group(function (){
	// admin routing
	Route::get('/', 'HomeController@index')->name('home');

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/my-profile', 'AdminController@show')->name('admin.my-profile');

	Route::get('/edit-profile', 'AdminController@edit')->name('admin.edit-profile');
	Route::post('/update-profile', 'AdminController@update')->name('admin.update-profile');

	// company routing
	Route::get('/company-list', 'CompanyController@index')->name('company.company-list');
	Route::get('/create-company', 'CompanyController@create')->name('company.create-company');
	Route::post('/save-company', 'CompanyController@save')->name('company.save-company');

	Route::get('/edit-company/{company_id}', 'CompanyController@edit')->name('company.edit-company');
	Route::post('/update-company-profile', 'CompanyController@update')->name('company.update-company-profile');

	Route::get('/delete-company/{company_id}', 'CompanyController@destroy')->name('company.delete-company');
	// Employee list
	Route::get('/employee-list', 'EmployeeController@index')->name('employee.employee-list');
	Route::get('/create-employee', 'EmployeeController@create')->name('employee.employee-create');
	Route::post('/save-employee', 'EmployeeController@save')->name('employee.save-employee');
	
	Route::get('/edit-employee-profile/{employee_id}', 'EmployeeController@edit')->name('employee.edit-profile');
	Route::post('/update-employee-profile', 'EmployeeController@update')->name('employee.update-employee-profile');

	Route::get('/delete-employee-profile/{employee_id}', 'EmployeeController@destroy')->name('employee.delete-employee-profile');
});

// Route::middleware(['auth','middleware' => 'App\Http\Middleware\CompanyMiddleware'])->group(function () {
// 	// Employee list
// 	Route::get('/employee-list', 'EmployeeController@index')->name('employee.employee-list');
// 	Route::get('/create-employee', 'EmployeeController@create')->name('employee.employee-create');
// 	Route::post('/save-employee', 'EmployeeController@save')->name('employee.save-employee');
	
// 	Route::get('/edit-employee-profile/{employee_id}', 'EmployeeController@edit')->name('employee.edit-profile');
// 	Route::post('/update-employee-profile', 'EmployeeController@update')->name('employee.update-employee-profile');

// 	Route::get('/delete-employee-profile/{employee_id}', 'EmployeeController@destroy')->name('employee.delete-employee-profile');
// });