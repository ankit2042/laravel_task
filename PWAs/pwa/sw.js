// Version
var appVersion = 'v1.01';

// File to cache
var files = [
	'./',
	'./index.html',
	'./style.css',
]
// Install:Cahche files
self.addEventListener('install',event =>{
	event.waitUntil(
		caches.open(appVersion)
		.then(cache=>{
			return cache.addAll(files)
			.catch(err=>{
				console.error('Error adding files to cache', err);
			})
		})
	)
	console.info('SW Installed');
	return self.skipWaiting();//change of website after refresh page
})
// Activate:manage old caches
self.addEventListener('activate', event=>{
	event.waitUntil(
		caches.keys()
		.then(cacheNames=>{
			return Promise.all(
				cacheNames.map(cache=>{
					if (cache!== appVersion){
						console.log("if deleting old cache",cache)
						return caches.delete(cache);
					}
				})
			) 
		})
	)
	return self.clients.claim();//change of website after refresh page
})
// Fetch: Control network request
self.addEventListener('fetch', event=>{
	console.info('SW fetch',event.request.url);
	event.respondWith(
		caches.match(event.request)
		.then(res=>{
			return res || fetch(event.request);
		})
	)
})