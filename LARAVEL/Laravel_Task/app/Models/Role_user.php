<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{   
    protected $table = "role_user";
	protected $fillable = [
	  'role_id','user_id',
  ];

    public const Admin = 'Admin';
    public const Company = 'Company';
    public const Employee = 'Employee';

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->using('App\Models\UserRole');
    }
    
  
}
