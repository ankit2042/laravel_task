<?php

namespace App\Http\Controllers;

use App\Notifications\Welcome_mail_notification;
use App\Notifications\DatabaseNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Role;
use Illuminate\Auth\Events\VerifiedEmail;
use Auth;
use DB;
use SESSION;

class EmployeeController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(Auth::user()->hasRole('Admin')){
      $employee_data = User::whereHas('roles', function($role) {
        $role->where('name', '=', Role::Employee);
      })->orderBy('name', 'ASC')->paginate(10);

      return view('admin.employee.view_employee_list')->with(['employee_data'=>$employee_data, 'users' => $employee_data]);
    }elseif(Auth()->user()->id){
      
      $logged_user_id = Auth()->user()->id;
      
      $employee_data = DB::table('users')
        ->join('roles', 'roles.id', '=', 'users.role')
        ->where('users.company', '=', $logged_user_id)
        ->orderBy('name', 'ASC')
        ->paginate(10,array('users.*'));

      return view('admin.employee.view_employee_list')->with(['employee_data'=>$employee_data, 'users' => $employee_data]);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $employee_data = DB::table('users')
                    ->join('roles', 'roles.id', '=', 'users.role')
                    ->where('users.role', '=', 2)
                    ->get('users.*');
    return view("admin.employee.create_employee")->with(['company_data'=>$employee_data]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function save(Request $request){
    $auth_user_id = Auth::User()->id;
    Validator::make($request->all(),
    [
      'first_name'   => 'required',
      'last_name'    => 'required',
      'email'        => 'required | email | string | max:255 | unique:users,email',
      'phone'        => 'required|min:11|numeric',
      'company_name' => 'not_in:Select Company',
      'password'     => ['required', 'string', 'min:6', 'confirmed']
    ],
    [
      'first_name.required'   => "first name is required",
      'last_name.required'    => "last name is required",
      'email.required'        => "Email is required",
      'email.email'           => "Valid email",
      'photo.required'        => "Image is required",
      'photo.image'           => "Image is not valid",
      'company_name.not_in'   => "Company name is required",
      'password.required'     => "Password is required.",
      'password.min'          => "Password length is minimum 6 character.",
      'password.confirmed'    => "Password does not match.",
    ])->validate();
    if ($auth_user_id == 1){
      $employee_crete = User::create([
        'name' => $request->input('first_name').' '.$request->input('last_name'),
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
        'email' => $request->input('email'),
        'website' => null,
        'password' => bcrypt($request->input('password')),
        'company' => $request->input('company_name'),
        'role' => $request->input('role'),
        'logo' => null,
        'phone' => $request->input('phone'),
      ]);
      $employee_crete->save();
      $employee_crete->addRole(Role::Employee);
      return redirect()->route('employee.employee-list');
    }else{
      $employee_crete = User::create([
        'name' => $request->input('first_name').' '.$request->input('last_name'),
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
        'email' => $request->input('email'),
        'website' => null,
        'password' => bcrypt($request->input('password')),
        'company' => $request->input('company_name'),
        'role' => $request->input('role'),
        'logo' => null,
        'phone' => $request->input('phone'),
      ]);
      $employee_crete->save();
      $employee_crete->notify(new Welcome_mail_notification);
      $employee_crete->addRole(Role::Employee);
      return redirect()->route('notify');
      return redirect()->route('employee.employee-list');
    } 
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id){
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id){
    $company_data = DB::table('users')
                        ->join('roles', 'roles.id', '=', 'users.role')
                        ->where('users.role', '=', 2)
                        ->get('users.*');
    $employee_data = User::findOrfail($id);
    return view('admin/employee/edit_employee_profile')->with(["employee_data"=>$employee_data,'company_name'=>$company_data]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request){
    // dd($request->input());  
    $employee_id = $request->input("employee_id");
    Validator::make($request->all(),
    [
      'first_name'   => 'required',
      'last_name'    => 'required',
      'phone'        => 'required|min:11|numeric',
    ],
    [
      'first_name.required'   => "first name is required",
      'last_name.required'    => "last name is required",
      'phone.required'        => "phone number is required"
    ])->validate();

    $employee_crete =User::find($employee_id)->update([
        'name' => $request->input('first_name').' '.$request->input('last_name'),
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
        'phone' => $request->input('phone'),  
    ]);
    return redirect()->route('employee.employee-list');    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($employee_id){
    $employee_id = request()->route('employee_id');
    $delete_employee_data = User::find($employee_id);

    $delete_employee_data->delete();

    return redirect()->route('employee.employee-list');
  }
}
