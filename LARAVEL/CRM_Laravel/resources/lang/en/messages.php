<?php

return [
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Login',
    'register'=> 'Resgister',
    'company' => 'Company',
    'employee' => 'Employee',
    'my profile' => 'My Profile',
    'logout' => 'Logout',
    'e-mail address'=> 'E-mail Address',
    'notification' => 'Notification',
    'dashboard' => 'Dashboard',
];