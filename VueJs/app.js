// make vue component
Vue.component("vuetest",{
    template:"<p>My name is {{ name }} . I have worked in {{ company }}</p>",
    data:function(){
        return {
            name:"Ankit",
            company:"Vkaps"
        }
    }
}); 
// make vue custom directive
Vue.directive('custom-directive',{
    bind:function(el,binding,vnode){
       console.log(el);
       console.log(binding);
       el.style.backgroundColor = "#aab8ac";
       el.fontSize = "50px";
       el.title = "update";
       el.style.color = "green";
       console.log("Value : "+binding.value);
    },
    update:function(){

    },
    unbind: function(){

    }
});
var form = new Vue({
        el: "#form",
        data: {
            aboutUs:"about-us.html",
            title:"This is my channel",
            name:"Ankit Purohit",
            PlaylistLink:"https://www.youtube.com/watch?v=BqCaoGuRDC4&list=PLT9miexWCpPUZ-xD9s70PhhP37blPSuax&index=4",
            ImageSrc:"https://images.unsplash.com/photo-1420593248178-d88870618ca0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80",
            email: "ankit.purohit@gmail.com",
            password:"123456789",
            htmlContent:"<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam ab dolor alias recusandae quod ipsa laborum quos optio, animi iure temporibus amet magnam laudantium! Nulla vitae iure quisquam cumque soluta!</p>",
            count:0,
            height:100,
            width :100
        },
        methods:{
            increment:function(increment){
                this.count += increment;
            },
            decrement:function(decrement){
                this.count -= decrement;
            },
            mouseMoveFunction:function(event){
                this.height = event.offsetX;
                this.width = event.offsetY
            },
            mouseoverFunction:function(){
                console.log("over");
            },
            mouseoutFunction:function(){
               console.log("mouse out");
            },
            keyPressFunction:function(event){
                console.log(event.key);
            },
            keyUpFunction:function(event){
                console.log(event);
            },
            keyDownFunction:function(){
                console.log("ankit");
            },
        }
    });
var newApp = new Vue({
        el:"#my-app",
        data:{
            aboutUs:"about-us.html"
        },
        methods:{

        }    
    });
    // about us page javascript
var about_us = new Vue({
    el:"#about-us",
    data:{
        firstName:"",
        lastName:"",
        meter:"",
        isActive:false,
        count:0,
        // for loop 
        students : ['Ankit','Vishu','Akshay','Yash','Nitin'],
    },
    methods:{
        toggleClass:function(){
            this.isActive = !this.isActive;
        },
        conditon:function(){
            var a = [this.count++,this.isActive = !this.isActive];
            return a;
        }
    },
    computed:{
        getFullName: function(){
            return this.firstName+" "+this.lastName;
        },
        convertCentimeter: function(){
            return this.meter*100;
        }

    }
});
// concept of references
var ref = new Vue({
    el:"#ref",
    data:{
        message:"This is custom directive",
    },
    methods:{
        btnSubmit:function(){
            console.log(this.$refs.txtname.value);
            console.log(this.$refs.txtmail.value);
        }
    },
    computed:{
        
    }    
})