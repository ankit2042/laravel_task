<?php

namespace App\Http\Controllers;

use App\Notifications\Welcome_mail_notification;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use SESSION;

class CompanyController extends Controller
{
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
     return view("admin.company.create_company");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function save(Request $request)
  {
      Validator::make($request->all(),
      [
        'name'  => 'required',
        'email' => 'required | email | string | max:255 | unique:users,email',
        'website' => 'required | string | max:255 | unique:users,website',
        'photo' => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048',
        'password' => ['required', 'string', 'min:6', 'confirmed'],
        'phone' => ['required', 'numeric']
      ],
      [
        'name.required' => "Name is required.",
        'email.required'=> "Email is required.",
        'email.email'   => "Valid email.",
        'photo.required'=> "Image is required.",
        'photo.image'   => "Image is not valid.",
        'password.required'   => "Password is required.",
        'password.min'   => "Password length is minimum 6 character.",
        'password.confirmed'   => "Password does not match.",
      ])->validate();

      if ($request->hasFile('photo')) {
         
        $image = $request->file('photo');
        $image_new_name = 'thumb_'.time().'.'.$image->extension();;
        $resize_image_destinationPath = storage_path('/resize_company_logo/');   
        $img = Image::make($image);        
        $img->resize(100, 100);
        $img->save($resize_image_destinationPath.$image_new_name);

        $origional_iamge_name = time().'.'.$image->extension();;
        $original_destinationPath = storage_path('/company_logo/');
        $image->move($original_destinationPath, $origional_iamge_name);
        
        $company_create = User::create([
            'name' => $request->input('name'),
            'first_name' => null,
            'last_name' => null,
            'email' => $request->input('email'),
            'website' => $request->input('website'),
            'password' => bcrypt($request->input('password')),
            'role' => $request->input('role'),
            'phone' => $request->input('phone'),
            'logo' => $image_new_name,
        ]);
        $company_create->save();

        $company_create->addRole(Role::Company);
        $company_create->notify(new Welcome_mail_notification); 
        return redirect()->route('notify');
        return redirect()->route('company.company-list');
    }
  }

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware(['auth', 'verified']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company_data = User::whereHas('roles', function($role) {
          $role->where('name', '=', Role::Company);
      })->paginate(10);
      $roles = Role::all();
      return view('admin/company/view_company_list')->with(['company_data'=>$company_data, 'users' => $company_data]);
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company_data = User::findOrfail($id);
        return view('admin/company/edit_company_profile')->with(["company_data"=>$company_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(),
        [
          'name'  => 'required',
          'website'  => 'required',
          'phone' => ['required', 'numeric'],
          'photo' => 'image | mimes:jpeg,png,jpg,gif,svg | max:2048',
        ],
        [
          'name.required'   => "Name is required",
          'website.required'=> "Website name  is required",
          'phone.required'  => "Phone number is required",
          'phone.numeric'   => "Valid phone number",
          'photo.mimes'     => "upload valid image formate",
        ])->validate();

        $company_id = $request->input('company_id');
        if($request->hasFile('photo')){

            $image = $request->file('photo');
            $image_new_name = 'thumb_'.time().'.'.$image->extension();
            // resize image upload to folder and database
            $resize_image_destinationPath = storage_path('/resize_company_logo/');   
            $img = Image::make($image);        
            $img->resize(100, 100);
            $img->save($resize_image_destinationPath.$image_new_name);
            // origional image upload to folder 
            $origional_iamge_name = time().'.'.$image->extension();;
            $original_destinationPath = public_path('/company_logo/');
            $image->move($original_destinationPath, $origional_iamge_name);
            
            User::find($company_id)->update([
                'name'  => $request->input('name'),
                'website'  => $request->input('website'),
                'phone'  => $request->input('phone'),
                'logo'  => $image_new_name
            ]);

              return redirect()->route('company.company-list'); 
        }else{
            User::find($company_id)->update([
              'name'  => $request->input('name'),
              'website'  => $request->input('website'),
              'phone'  => $request->input('phone'),
            ]);

            return redirect()->route('company.company-list');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($company_id)
    {
        $company_id = request()->route('company_id');
        $delete_company_data = User::find($company_id);
        $delete_company_data->delete();

        return redirect()->route('company.company-list');
    }
}
