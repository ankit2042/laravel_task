<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;


class CompanyModel extends Model{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  public $table = "users";
  
  protected $primaryKey = 'id';
  
  protected $fillable = [
        'name',
        'email',
        'password',
        'image',
        ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
      'email_verified_at' => 'datetime',
  ];

  public function role(){
    Schema::table('role_user', function ($table)  {
      return $this->belongsTo("App\Models\UserRole");
    });
  }
  public function hasRole(String $roleName)
  {
      return $this->roles()->where('name', $roleName)->exists();
  }
  /**
   ** The roles that belong to the user.
  **/
  public function roles()
  {
    return $this->belongsToMany('App\Models\UserRole');
  }

  public function addRole(String $roleName)
  {
    $role = Role::where('name', $roleName)->first();
    /*dd($role);
    if ($role){
      echo "if"; die;
    }else{
      echo "else";
    }*/ $this->roles()->save($role);
  } 
 
}
