@extends('layouts.app')

@section("content")
<section class="container">
	<div class="row">
		<div class="col-md-10 mx-1 mx-auto">
			<div class="card">
				<div class="card-header"><h5 class="d-inline float-left">{{ __('Company Employee List') }}</h5>
					<a href="{{route('employee.employee-create')}}" class="btn btn-sm btn-success float-right">Add Employee</a>
				</div>
				<div class="card-body">
					<table class="table table-dark table-hover table-bordered ">
						<tr>
							<th>S No</th>
							<th>Employee Name</th>
							<th>Email Address</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
						
						@php $i = 1 @endphp
						@forelse ($employee_data as $employee_data)
							<tr>
								<td>{{ $i }}</td>
								<td>{{$employee_data->name}}</td>
								<td>{{$employee_data->email}}</td>
								<td><a href="/edit-employee-profile/{{ $employee_data->id }}" class="btn btn-sm btn-success">Edit</a></td>
								<td><a href="/delete-employee-profile/{{ $employee_data->id }}" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger">Delete</a></td>
							</tr>
						@php $i++ @endphp
						@empty
							<td colspan="7" class="text-danger">No employee exixts</td>
						@endforelse

					</table>
					<td>{{ $users->links() }}</td>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection