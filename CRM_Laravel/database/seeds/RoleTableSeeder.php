<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
	    $role_admin->name = 'admin';
	    $role_admin->save();

	    $role_company = new Role();
	    $role_company->name = 'company';
	    $role_company->save();
	    
	    $role_employee = new Role();
	    $role_employee->name = 'employee';
	    $role_employee->save();
    }
}
