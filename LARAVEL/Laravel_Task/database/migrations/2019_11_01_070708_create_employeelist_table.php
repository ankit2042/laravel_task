<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeelistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employeelist', function (Blueprint $table) {
            $table->bigIncrements('employee_id');
            $table->string('employee_first_name');
            $table->string('employee_last_name');
            $table->string('employee_email')->unique();
            $table->bigInteger('employee_phone');
            $table->string('company_name');
            $table->timestamps();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employeelist');
    }
}
