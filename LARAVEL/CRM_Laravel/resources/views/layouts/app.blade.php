<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta charset="utf-8" name="user-id" content="{{Auth::check() ? Auth::user()->id : ''}}">
      <title>{{ config('app.name', 'Mini CRM') }}</title>
      <!-- Font awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   </head>
<body>
  <div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/home') }}">
          {{ 'Mini CRM' }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          @auth
            @if (Auth::user()->hasRole('Admin'))
              <ul class="navbar-nav mr-auto top-right links">
                <li><a href="{{ url('/company-list') }}" class="nav-link">{{__('auth.company')}}</a></li>
                <li><a href="{{ url('/employee-list') }}" class="nav-link">{{__('auth.employee')}}</a></li>
              </ul>
              @elseif(Auth::user()->hasRole('Company'))
                <ul class="navbar-nav mr-auto top-right links">
                  <!-- <li><a href="{{ url('/company-list') }}" class="nav-link">Company</a></li> -->
                  <li><a href="{{ url('/employee-list') }}" class="nav-link">{{__('auth.employee')}}</a></li>
                </ul>
              @else
                <ul class="navbar-nav mr-auto top-right links">
                  <a href="#">My Profile</a>
                  <!-- <li><a href="{{ url('/company-list') }}" class="nav-link">Company</a></li> -->
                  <!-- <li><a href="{{ url('/employee-list') }}" class="nav-link">Employee</a></li> -->
                </ul>
              @endif
            @endauth
            @if(Route::current()->getName() == 'admin')
              <!-- {{-- adnin right bar --}} -->
            @else
              <!--{{-- Right Side Of Navbar --}}-->
              <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
            @guest
              <!-- @if (Route::has('login'))
                <div class="top-right links">
                  @auth
                    <a href="{{ url('/home') }}">Home</a>
                  @else
                    <a href="{{ route('login') }}">Login</a>
                   
                    @if (Route::has('register'))
                      <a href="{{ route('register') }}">Register</a>
                    @endif
                  @endauth
                </div>
              @endif  -->
            @else
            <!-- {{--  notification dropdown --}} -->
            @if(Auth::user()->hasRole('Employee') || Auth::user()->hasRole('Company'))
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <span class="badge badge-danger notification" id="">{{Auth::user()->unreadNotifications->count()}}</span>
                <span class="caret"><i class="fa fa-bell"></i></span>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                
                @foreach (Auth::user()->notifications as $notifications) 
                  @if(Auth::user()->unreadNotifications)
                    <a class="dropdown-item text-dark notify" href="{{route('markAsRead')}}">
                    </a>
                  @endif   
                @endforeach
                <a href="{{route('mark-all-read-notification')}}" class="">
                      All read notification
                    </a> 
              </div>
            </li>
            @endif
            <!--{{-- End Notification dropdown--}} -->
            <!--{{-- Start Admin Dropdown--}}-->
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{route('admin.my-profile')}}"> {{__('messages.my profile')}} </a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                  {{__('messages.logout')}}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
            <!-- {{-- End admin drop down --}} -->
            @endguest
            <!-- {{-- language coding --}} -->
            <ul class="navbar-nav ml-auto">
              <li class="nav-item dropdown">
                @if (Session::has('locale'))
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{session('locale')}}<span class="caret"></span>
                </a>
                @else
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{Config::get('app.locale')}}<span class="caret"></span>
                </a>
                @endif
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                   <span><a href="{{ url('lang/en') }}" class="dropdown-item"><i class="fa fa-language"></i> EN</a></span>
                   <span><a href="{{ url('lang/fr') }}" class="dropdown-item"><i class="fa fa-language"></i> FR</a></span>
                </div>
              </li>
            </ul>
          @endif
        </div>
      </div>
    </nav>
    <!-- content coding -->
    <main class="py-4">
      @yield('content')
    </main>
  </div>
</body>
</html>