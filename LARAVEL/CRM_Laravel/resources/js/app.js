  /**
   * First we will load all of this project's JavaScript dependencies which
   * includes Vue and other libraries. It is a great starting point when
   * building robust, powerful web applications using Vue and Laravel.
   */

  require('./bootstrap');

  window.Vue = require('vue');

  /**
   * The following block of code may be used to automatically register your
   * Vue components. It will recursively scan this directory for the Vue
   * components and automatically register them with their "basename".
   *
   * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
   */

  // const files = require.context('./', true, /\.vue$/i)
  // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

  Vue.component('example-component', require('./components/ExampleComponent.vue').default);

  /**
   * Next, we will create a fresh Vue application instance and attach it to
   * the page. Then, you may begin adding components to this application
   * or customize the JavaScript scaffolding to fit your unique needs.
   */

  const app = new Vue({
      el: '#app',
  });

  //get user value
  let userId = document.head.querySelector('meta[name="user-id"]').content;

  // show broadcast message
  Echo.private('App.Models.User.' + userId).notification((notification) => {
  	document.querySelector('.notification').innerHTML = notification.count;
  });
  // fetch notification from data without refreshing page
  function loadData() {
    $.ajax({
      type: 'GET',
      url: '/get-notification/',
      success: function(value){
        var response  = JSON.parse(value);
        var data  = JSON.parse(response.data);
        if (response['status'] == 1) {
          $.each(data, function(key, value) {
            $(".notify").text(value);
          });
        }
      }
    });
  };
  // get data from notification table in database
  setInterval(function(){
      loadData();
  },500);
