@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Edit Employee') }}</div>

        <div class="card-body">
          <form method="post" action="/update-employee-profile">
            @csrf
            <input type="hidden" name="employee_id" value="{{$employee_data->employee_id}}">
            <div class="form-group row">
              <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('First name') }}</label>

              <div class="col-md-6">
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{$employee_data->employee_first_name}}">

                @error('first_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last name') }}</label>

              <div class="col-md-6">
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{$employee_data->employee_last_name}}">

                @error('last_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="company_name" class="col-md-4 col-form-label text-md-right">{{ __('Company name') }}</label>

              <div class="col-md-6">
                <select name="company_name" class="form-control @error('company_name') is-invalid @enderror">
                  <option>Select Company</option>
                  @forelse ($company_name as $company_name)
                    <option value="{{$company_name->company_id}}"> {{$company_name->company_name}} </option>
                  @empty
                    <option>No company found</option>
                  @endforelse
                </select>

                @error('company_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$employee_data->employee_email}}" disabled="">

                @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

              <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$employee_data->employee_phone}}">

                @error('phone')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary btn-sm">
                  {{ __('Update Employee') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
