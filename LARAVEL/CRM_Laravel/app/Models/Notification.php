<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Notification extends Model
{
	protected $table="notifications";

	public function get_notification(){
		$user_id = Auth::user()->id;
		$data =  DB::table('notifications')->select('data')->where('notifiable_id', $user_id)->get();
		return $data;
	}
}
