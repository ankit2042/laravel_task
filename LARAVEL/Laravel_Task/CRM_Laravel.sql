-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 08, 2019 at 08:16 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CRM_Laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_08_062413_create_roles_table', 1),
(5, '2019_11_08_062608_create_user_roles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2019-11-08 07:41:42', '2019-11-08 07:41:42'),
(2, 'Company', '2019-11-08 07:41:42', '2019-11-08 07:41:42'),
(3, 'Employee', '2019-11-08 07:41:49', '2019-11-08 07:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '2019-11-08 09:45:25', '2019-11-08 09:45:25'),
(2, '2', '2', '2019-11-08 09:45:25', '2019-11-08 09:45:25'),
(3, '3', '3', '2019-11-08 09:45:45', '2019-11-08 09:45:45'),
(5, '5', '3', '2019-11-08 09:45:45', '2019-11-08 09:45:45'),
(6, '6', '3', '2019-11-08 10:54:37', '2019-11-08 10:54:37'),
(7, '11', '2', '2019-11-08 11:38:35', '2019-11-08 11:38:35'),
(8, '13', '2', '2019-11-08 11:47:13', '2019-11-08 11:47:13'),
(9, '15', '2', '2019-11-08 12:06:10', '2019-11-08 12:06:10'),
(10, '18', '2', '2019-11-08 12:30:35', '2019-11-08 12:30:35'),
(11, '19', '3', '2019-11-08 14:28:09', '2019-11-08 14:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `website`, `email_verified_at`, `password`, `company`, `role`, `logo`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin Name', NULL, NULL, 'admin@admin.com', NULL, NULL, '$2y$10$sc04mdHh8S.Shqes7Gn/.uIP1DitHCpfwhc4cn5LF0YOPvRZwe9xO', NULL, '1', NULL, NULL, '0RuSHj15MQJ7XSke5YVRxIN5h9yEJ5NquvPfn8h5FMjkgxSTrZ9jXr78zpIW', '2019-11-08 02:10:18', '2019-11-08 02:10:18'),
(2, 'Company Name', NULL, NULL, 'company@admin.com', 'Company.com', NULL, '$2y$10$sc04mdHh8S.Shqes7Gn/.uIP1DitHCpfwhc4cn5LF0YOPvRZwe9xO', NULL, '3', 'thumb_1573219458.png', '9522749437', 'eUGFZkbqDTRKGnqjsIHp0pIhXSWjVGVOCPdLd3oW26oFD8TE5kQq3ixGKspu', '2019-11-08 02:10:18', '2019-11-08 07:54:18'),
(3, 'compant name', NULL, NULL, 'ankit.purohit616@gmail.com', NULL, NULL, '$2y$10$l.JUWNSaTNaR88oPTbPwEufqVosIwyXYHWPK4.Gvg3C1xbsjDm/RW', NULL, '2', NULL, NULL, NULL, '2019-11-08 04:46:03', '2019-11-08 04:46:03'),
(5, 'Ankit Purohit', NULL, NULL, 'ankit.pdxvurohit616@gmail.com', NULL, NULL, '$2y$10$l.JUWNSaTNaR88oPTbPwEufqVosIwyXYHWPK4.Gvg3C1xbsjDm/RW', NULL, '2', NULL, NULL, NULL, '2019-11-08 04:46:03', '2019-11-08 04:46:03'),
(6, 'Company Name', NULL, NULL, 'company@admifgxn.com', NULL, NULL, '$2y$10$sc04mdHh8S.Shqes7Gn/.uIP1DitHCpfwhc4cn5LF0YOPvRZwe9xO', NULL, '2', NULL, NULL, NULL, '2019-11-08 02:10:18', '2019-11-08 02:10:18'),
(11, 'GPH', NULL, NULL, 'palash@geeksperhour.com', 'vkaps.com', NULL, '$2y$10$foBJOn.u/.bTIZHiDS22YeYduYvTAvouNPIIXJk3aIA7v9bCHuQ1K', NULL, '2', 'thumb_1573219434.jpeg', '112346879', 'w0c8kWkF40YhCvhrZW7SZ2xHw0IexyQbNMpedHFPyqfgQIOpNCkEgrZUkRXE', '2019-11-08 06:08:35', '2019-11-08 07:53:54'),
(13, 'Vkaps It Solution', NULL, NULL, 'hr@vkaps.com', 'Vkaps.conm', NULL, '$2y$10$F.FRQL9FMc.A7vDyPVy3oeUl.Pkw/yD5cEjR5rasfj99ZMNsY57mC', NULL, '2', 'thumb_1573219422.png', '134579', NULL, '2019-11-08 06:17:13', '2019-11-08 07:53:42'),
(15, 'Future Technology', NULL, NULL, 'ritu@geeksperhour.com', 'FutureTechnology.com', NULL, '$2y$10$jBoED2/3UfyP6sGTZGcxFuvlUmxmiL14Tb6Xb7vi8JL1ym2BL6pSy', NULL, '2', 'thumb_1573219358.jpeg', '8602621409', NULL, '2019-11-08 06:36:10', '2019-11-08 07:52:48'),
(19, 'kishan kewat', 'kishan', 'kewat', 'kishan@geeksperhour.com', NULL, NULL, '$2y$10$HUAOpEh8h1IKdCC6M6N92uHPBjKw0et6vLGmzUdhNioi816haXspO', '2', '3', NULL, '02145646466', NULL, '2019-11-08 08:58:09', '2019-11-08 08:58:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_website_unique` (`website`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
