<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'admin')->first();
	    $role_company  = Role::where('name', 'company')->first();
	    $role_employee  = Role::where('name', 'employee')->first();
	    
	    $admin = new User();
	    $admin->name = 'admin Name';
	    $admin->email = 'admin@admin.com';
	    $admin->password = bcrypt('password');
	    $admin->role = '1';
	    $admin->save();
	    $admin->UserRole()->attach($role_admin);
	    
	    $company = new User();
	    $company->name = 'company Name';
	    $company->email = 'company@company.com';
	    $company->password = bcrypt('123456');
	    $company->role = '2';
	    $company->save();
	    $company->UserRole()->attach($role_company);

	    $employee = new User();
	    $employee->name = 'employee Name';
	    $employee->email = 'employee@employee.com';
	    $employee->password = bcrypt('123456');
	    $employee->role = '3';
	    $employee->save();
	    $employee->UserRole()->attach($role_employee);
    }
}
