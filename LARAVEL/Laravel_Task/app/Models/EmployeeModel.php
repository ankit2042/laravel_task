<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;


class EmployeeModel extends Authenticatable{

  use Notifiable;

  protected $guard = 'employee';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  public $table = "employeelist";
  
  protected $primaryKey = 'employee_id';
  
  protected $fillable = [
	  'employee_first_name','employee_last_name', 'employee_email', 'employee_phone','company_name',
  ];
 
}
