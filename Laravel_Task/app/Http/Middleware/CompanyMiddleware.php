<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Models\Role;

class CompanyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->hasRole(Role::Admin)){
            return $next($request);
        }elseif($request->user() && $request->user()->hasRole(Role::Company)){
           return $next($request);
        }else{
            return $next($request);
        }
        return redirect('/')->with('forbidden', 'You are not permitted to access this page');
    }
}
