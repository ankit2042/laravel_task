<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Role;
use App\Models\CompanyModel;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Pagination\Paginator;
use DB;

class CompanyController extends Controller{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(){
    $company_data = User::whereHas('roles', function($role) {
            $role->where('name', '=','Company');
        })->paginate(1);

        /*$clients = User::whereHas('roles', function($role) {
            $role->where('name', '=', Role::CLIENT);
        })->with(['roles' => function($role) {
            $role->where('name', '=', Role::CLIENT);
        }])->get();*/

        $roles = Role::all();
    return view('admin/company/view_company_list')->with(['company_data'=>$company_data, 'users' => $company_data]);

  }
  

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(){
    return view("admin/company/create_company");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function save(Request $request){
    // server side validation for company register form
    Validator::make($request->all(),
    [
      'name'  => 'required | unique:companylist,company_name',
      'email' => 'required | email | string | max:255 | unique:companylist,company_email',
      'photo' => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048',
      'password' => ['required', 'string', 'min:6', 'confirmed']
    ],
    [
      'name.required' => "Name is required.",
      'email.required'=> "Email is required.",
      'email.email'   => "Valid email.",
      'photo.required'=> "Image is required.",
      'photo.image'   => "Image is not valid.",
      'password.required'   => "Password is required.",
      'password.min'   => "Password length is minimum 6 character.",
      'password.confirmed'   => "Password does not match.",
    ])->validate();
    // company logo upload
    $image = $request->file('photo');
    $iamge_new_name = 'thumb_'.time().'.'.$image->extension();
    
    // resize upload image
    $resize_image_destinationPath = public_path('/resize_company_logo/');
    $img = Image::make($image);
    $img->resize(100, 100);
    $img->save($resize_image_destinationPath.$iamge_new_name);

    
    $company_create = CompanyModel::create([
      'name'  => $request->input('name'),
      'email' => $request->input('email'),
      'image'  => $iamge_new_name,
      'password'  => bcrypt($request->input('password')),
    ]);
    $company_create->save();

      $company_create->addRole(Role::Company);
    // original image upload
      die("end");
    $origional_iamge_name = time().'.'.$image->extension();
    $original_destinationPath = public_path('/company_logo/');
    $image->move($original_destinationPath, $origional_iamge_name);
    return redirect()->route('company.company-list'); 
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\CompanyModel  $companyModel
   * @return \Illuminate\Http\Response
   */
  public function show(CompanyModel $companyModel){
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\CompanyModel  $companyModel
   * @return \Illuminate\Http\Response
   */
  public function edit(Request $request){
    $company_id = request()->route('company_id');
    $company_data = User::findOrfail($company_id);
    return view('admin/company/edit_company_profile')->with(["company_data"=>$company_data]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\CompanyModel  $companyModel
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, CompanyModel $companyModel){
    $company_id = $request->input('company_id');
    if ($request->hasFile('photo')) {
      Validator::make($request->all(),
      [
        'name'  => 'required',
        'photo' => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048'
      ],
      [
        'name.required' => "Name is required",
        'photo.required'=> "Image is required",
        'photo.image'   => "Image is not valid"
      ])->validate();
      
      $image = $request->file('photo');
      $iamge_new_name = 'thumb_'.time().'.'.$image->extension();
      
      // resize upload image
      $resize_image_destinationPath = public_path('/resize_company_logo/');
      $img = Image::make($image);
      $img->resize(100, 100);
      $img->save($resize_image_destinationPath.$iamge_new_name);

      CompanyModel::find($company_id)->update([
        'company_name'  => $request->input('name'),
        'company_logo'  => $iamge_new_name
      ]);

      return redirect()->route('company.company-list');
      
    }else{
      Validator::make($request->all(),
      [
        'name'  => 'required',
      ],
      [
        'name.required' => "Name is required",
      ])->validate();
      
      CompanyModel::find($company_id)->update([
        'company_name'  => $request->input('name'),
      ]);

      return redirect()->route('company.company-list');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\CompanyModel  $companyModel
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request ,CompanyModel $companyModel)
  {
    $company_id = request()->route('company_id');
    $delete_company_data = CompanyModel::find($company_id);

    $delete_company_data->delete();

    return redirect()->route('company.company-list');
  }
}
