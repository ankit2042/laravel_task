<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserRole;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'admin Name';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('password');
        $admin->role = '1';
        $admin->save();
        $admin->UserRole()->attach($role_admin);
        
        $company = new User();
        $company->name = 'company Name';
        $company->email = 'company@company.com';
        $company->password = bcrypt('123456');
        $company->role = '2';
        $company->save();
        $company->UserRole()->attach($role_company);

        $employee = new User();
        $employee->name = 'employee Name';
        $employee->email = 'employee@employee.com';
        $employee->password = bcrypt('123456');
        $employee->role = '3';
        $employee->save();
        $employee->UserRole()->attach($role_employee); 
        // $this->call(UsersTableSeeder::class);
        // $this->call(RolesTableSeeder::class);
    }
}
