<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\EmployeeModel::class, function (Faker $faker) {
  return [
    'employee_first_name' => $faker->name,
    'employee_last_name' => $faker->name,
    'employee_email' => $faker->unique()->safeEmail,
    'employee_phone' => $faker->numberBetween(1,100),
    'company_name' => $faker->randomDigit(2),
  ];
});

$factory->define(App\Models\CompanyModel::class, function (Faker $faker) {
  return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        'role'  => 'Company',
        'image'  => 'null',
        'remember_token' => Str::random(10),
    ];
});

$factory->define(App\Post::class, function (Faker $faker) {
  return [
    'title' => $faker->name,
    'body' => $faker->text,
  ];
});