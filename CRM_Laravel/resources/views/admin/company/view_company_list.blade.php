@extends('layouts.app')

@section("content")
<section class="container">
	<div class="row">
		<div class="col-md-10 mx-1 mx-auto">
			<div class="card">
				<div class="card-header"><h5 class="d-inline float-left">{{ __('Company List') }}</h5>
					<a href="{{route('company.create-company')}}" class="btn btn-sm btn-success float-right">Add Company</a>
				</div>
				<div class="card-body">
					<table class="table table-dark table-hover table-bordered">
						<tr>
							<th>S No</th>
							<th>Company Name</th>
							<th>Email Address</th>
							<th>Company logo</th>
							<th>Edit</th>
							<th>Delete</th>

						</tr>
						@php $i = 1 @endphp
						@forelse ($company_data as $company_data)
						{{$company_data->logo}}
							<tr>
								<td>{{$i}}</td>
								<td>{{$company_data->name}}</td>
								<td>{{$company_data->email}}</td>

								<!-- <td><img src="{{ storage_path('resize_company_logo/').$company_data->logo}}"></td> -->
								<td><img src="{{storage_path().'/resize_company_logo/'.$company_data->logo}}"></td>
								<td><a href="/edit-company/{{$company_data->id}}" class="btn btn-sm btn-success">Edit</a></td>
								<td><a href="/delete-company/{{$company_data->id}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')">Delete</a></td>
							</tr>
						@php $i++ @endphp
						@empty
							<td colspan="6"><p class="text-danger text-center">No data exits</p></td>
						@endforelse
						
					</table>
					<p>{{$users->links()}}</p>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection