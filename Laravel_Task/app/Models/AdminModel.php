<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RoleModel;
use App\Models\Role;

class AdminModel extends Model
{
	public function hasRole(String $roleName)
    {
        return $this->roles()->where('name', $roleName)->exists();
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }
    public function addRole(String $roleName)
    {
        $role = Role::where('name', $roleName)->first();

        if ($role) $this->roles()->save($role);
    }   
}
