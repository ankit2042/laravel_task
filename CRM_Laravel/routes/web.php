<?php
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Notification;
use App\Models\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('admin', ['as'=>'admin', function() {
    return view('auth/login');
}]);

# routes/web.php

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback','Auth\LoginController@handleProviderCallback');

Auth::routes(['verify'=>true]);

Route::get('/verify/{token}', 'EmployeeController@verifyUser')->name('ankit');
// language route
Route::get('lang/{lang}',function($lang){
	// get language in session
	\Session::put('locale',$lang);
	return redirect()->back();
})->middleware('language');
Route::middleware(['auth','language','verified','middleware' => 'App\Http\Middleware\UserRoleMiddleware'])->group(function (){
	
	Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
	
	// notification route
	Route::get('mark-all-read','NotificationController@mark_all_read_notification')->name('mark-all-read-notification');
	Route::get('notify','HomeController@notify')->name('notify');
	Route::get('get-notification','NotificationController@get_notification')->name('get-notification');
	Route::get('markAsRead','NotificationController@markAsRead')->name('markAsRead');

	// admin routing
	Route::get('/', 'HomeController@index')->name('home');

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/my-profile', 'AdminController@show')->name('admin.my-profile');

	Route::get('/edit-profile', 'AdminController@edit')->name('admin.edit-profile');
	Route::post('/update-profile', 'AdminController@update')->name('admin.update-profile');

	// company routing
	Route::get('/company-list', 'CompanyController@index')->name('company.company-list');
	Route::get('/create-company', 'CompanyController@create')->name('company.create-company');
	Route::post('/save-company', 'CompanyController@save')->name('company.save-company');

	Route::get('/edit-company/{company_id}', 'CompanyController@edit')->name('company.edit-company');
	Route::post('/update-company-profile', 'CompanyController@update')->name('company.update-company-profile');

	Route::get('/delete-company/{company_id}', 'CompanyController@destroy')->name('company.delete-company');
	// Employee list
	Route::get('/employee-list', 'EmployeeController@index')->name('employee.employee-list');
	Route::get('/create-employee', 'EmployeeController@create')->name('employee.employee-create');
	Route::post('/save-employee', 'EmployeeController@save')->name('employee.save-employee');
	
	Route::get('/edit-employee-profile/{employee_id}', 'EmployeeController@edit')->name('employee.edit-profile');
	Route::post('/update-employee-profile', 'EmployeeController@update')->name('employee.update-employee-profile');

	Route::get('/delete-employee-profile/{employee_id}', 'EmployeeController@destroy')->name('employee.delete-employee-profile');
	
	// notification-read route
	Route::get('notification-read','NotificationController@notification_read')->name('notification-read');
});