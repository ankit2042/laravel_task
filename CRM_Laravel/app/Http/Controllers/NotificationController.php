<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Notification;
use DB;

class NotificationController extends Controller{
  
  public function notification_read(){
    Auth::user()->notifications->markAsRead();
    return redirect()->back();
  }

  public function markAsRead(){
    \Auth::user()->notifications->markAsRead();
    return redirect()->back();
  }
  public function mark_all_read_notification(){
    $notifiable_id = Auth::user()->id;
	  $delete_employee_data = DB::table('notifications')->where('notifiable_id',$notifiable_id)->delete();
    return redirect()->back();
  }

  public function get_notification(){
    $user_id = Auth::user()->id;
    $notification = new Notification;
    $notification_data = $notification->get_notification();
    $new_notification_data = [];
    foreach ($notification_data as $key => $value) {
      $new_notification_data[$key] = json_decode($notification_data[$key]->data)->letter->title;
    }
    // print_r($new_notification_data); die;
    echo $bb = json_encode(array("status"=>"1","response"=>"success","data"=>json_encode($new_notification_data,JSON_FORCE_OBJECT)));
  }
}