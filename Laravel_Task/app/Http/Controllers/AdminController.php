<?php

namespace App\Http\Controllers;

// use App\Models\AdminModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Models\Role;
use Auth;

class AdminController extends Controller{
  
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:admin');
        // $this->middleware('auth:company');
        // $this->middleware('auth:employee');
    }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(){
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\AdminModel  $adminModel
   * @return \Illuminate\Http\Response
   */
  public function show(){
    return view("admin/my_profile");
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\AdminModel  $adminModel
   * @return \Illuminate\Http\Response
   */
  public function edit(){
    return view('admin/edit_admin_profile');
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\AdminModel  $adminModel
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request){
    $admin_id = $request->input('admin_id');

    Validator::make($request->all(),
    [
      'name'=>'required',
    ],
    [
      'name.required' => "Name is required",
    ])->validate();

    $update_user = user::find($admin_id)->update([
      'name' => $request->input('name'),
    ]);

    return redirect()->route('admin.my-profile');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\AdminModel  $adminModel
   * @return \Illuminate\Http\Response
   */
  public function destroy(AdminModel $adminModel){
      //
  }
}
