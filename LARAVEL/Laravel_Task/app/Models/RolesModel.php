<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyModel;

class RolesModel extends Model
{
  protected $fillable = [
		'role_name'
	];

	public function Company(){
    	return $this->hasMany("App\Models\CompanyModel");
  	}

  	public function users()
	{
	  return $this->belongsToMany(User::class);
	}	
}
