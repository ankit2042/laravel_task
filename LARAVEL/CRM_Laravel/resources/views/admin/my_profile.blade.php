@extends('layouts.app')

@section('content')

<section class="container">
	<div class="row">
		<div class="col-md-6 mx-auto">
			<div class="card">
				<div class="card-header">
					<h4 class="font-weight-bolder m-0">My Profile</h4>
				</div>
				<div class="card-body">
					<table class="table table-bordered m-0 p-0">
						@if (Route::has('login'))
							@auth
								<tr>
									<th>Name : </th>
									<td>{{auth()->user()->name}}</td>
								</tr>
								<tr>
									<th>Email : </th>
									<td>{{auth()->user()->email}}</td>
								</tr>
							@endauth
							@else
								@php route('/') @endphp
							@endif 
					</table>
				</div>
				<div class="card-footer">
					<a href="edit-profile" class="btn btn-success btn-sm">Edit </a>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection