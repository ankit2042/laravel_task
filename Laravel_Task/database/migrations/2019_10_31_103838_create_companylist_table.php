<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanylistTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up(){
    Schema::create('companylist', function (Blueprint $table) {
      $table->bigIncrements('company_id');
      $table->string('company_name');
      $table->string('company_email')->unique();
      $table->string('company_logo');
      $table->string('role')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down(){
    Schema::dropIfExists('companylist');
  }
}
