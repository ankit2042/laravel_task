<?php

namespace App\Http\Controllers;

use App\Models\EmployeeModel;
use App\Models\CompanyModel;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Http\MiddleWare\CompanyMiddleware;

class EmployeeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
   $employee_data = DB::table('users')
            ->leftJoin('companylist', 'users.role', '=', 'companylist.company_id')
            ->take('users')->paginate(10);
           
    return view('admin/employee/view_employee_list')->with(['employee_data'=>$employee_data,"users"=>$employee_data]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $company_name = CompanyModel::all();
    // dd($company_name);
    return view('admin/employee/create_employee')->with(['company_name'=>$company_name]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function save(Request $request)
  {
    Validator::make($request->all(),
    [
      'first_name'   => 'required',
      'last_name'    => 'required',
      'email'        => 'required | email | string | max:255 | unique:employeelist,employee_email',
      'phone'        => 'required|min:11|numeric',
      'company_name' => 'not_in:Select Company'
    ],
    [
      'first_name.required'   => "first name is required",
      'last_name.required'    => "last name is required",
      'email.required'        => "Email is required",
      'email.email'           => "Valid email",
      'photo.required'        => "Image is required",
      'photo.image'           => "Image is not valid",
      'company_name.not_in'   => "Company name is required"
    ])->validate();

    EmployeeModel::create([
      'employee_first_name'  => $request->input('first_name'),
      'employee_last_name'   => $request->input('last_name'),
      'employee_email'       => $request->input('email'),
      'employee_phone'       =>$request->input('phone'),
      'company_name'         =>$request->input('company_name'),
    ]);

    return redirect()->route('employee.employee-list');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\EmployeeModel  $employeeModel
   * @return \Illuminate\Http\Response
   */
  public function show(EmployeeModel $employeeModel)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\EmployeeModel  $employeeModel
   * @return \Illuminate\Http\Response
   */
  public function edit(Request $request , EmployeeModel $employeeModel)
  {
    $company_name = CompanyModel::all();

    $employee_id = request()->route('employee_id');

    $employee_data = EmployeeModel::findOrfail($employee_id);
    return view('admin/employee/edit_employee_profile')->with(['employee_data'=>$employee_data,'company_name'=>$company_name]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\EmployeeModel  $employeeModel
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, EmployeeModel $employeeModel)
  {
    $employee_id = $request->input('employee_id');
    Validator::make($request->all(),
    [
      'first_name'   => 'required',
      'last_name'    => 'required',
      'phone'        => 'required|min:11|numeric',
      'company_name' => 'not_in:Select Company'
    ])->validate();

    EmployeeModel::find($employee_id)->update([
        'employee_first_name'  => $request->input('first_name'),
        'employee_last_name'  => $request->input('last_name'),
        'company_name'  => $request->input('company_name'),
        'employee_phone'  => $request->input('phone'),
      ]);
    
    return redirect()->route('employee.employee-list');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\EmployeeModel  $employeeModel
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request,EmployeeModel $employeeModel)
  {
    $employee_id = request()->route('employee_id');
    $delete_employee_data = EmployeeModel::find($employee_id);

    $delete_employee_data->delete();

    return redirect()->route('employee.employee-list');
  }
}
